# VUE CLI 3 PRESET for EASY Client Framework 3.0 (CFW)
```
Use this preset for start a new client project.
```

- More information and how to use/create [vue cli 3 presets](https://cli.vuejs.org/dev-guide/plugin-dev.html#generator)  
- More information about [vue](https://vuejs.org/)

## This project is still under heavy construction, use it only for testing purpose
